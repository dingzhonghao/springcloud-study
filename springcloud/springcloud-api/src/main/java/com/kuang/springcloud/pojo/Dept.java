package com.kuang.springcloud.pojo;



import java.io.Serializable;
import java.util.Objects;

public class Dept implements Serializable {

    private Long deptno;
    private String dname;

    // 这个数据数存在哪个数据库的字段
    // 微服务，一个服务对应一个数据库，同一个信息可能存在不能的数据库
    private String db_source;

    public Long getDeptno() {
        return deptno;
    }

    public void setDeptno(Long deptno) {
        this.deptno = deptno;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getDb_source() {
        return db_source;
    }

    public void setDb_source(String db_source) {
        this.db_source = db_source;
    }

    public Dept() { }

    public Dept(Long deptno, String dname, String db_source) {
        this.deptno = deptno;
        this.dname = dname;
        this.db_source = db_source;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dept)) return false;
        Dept dept = (Dept) o;
        return Objects.equals(getDeptno(), dept.getDeptno()) &&
                Objects.equals(getDname(), dept.getDname()) &&
                Objects.equals(getDb_source(), dept.getDb_source());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDeptno(), getDname(), getDb_source());
    }

    @Override
    public String toString() {
        return "Dept{" +
                "deptno=" + deptno +
                ", dname='" + dname + '\'' +
                ", db_source='" + db_source + '\'' +
                '}';
    }


}
