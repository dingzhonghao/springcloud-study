package com.kuang.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;

public class KuangRule {
    @Bean
    public IRule myRule(){
        return new RoundRobinRule();
    }
}
